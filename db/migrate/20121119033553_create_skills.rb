class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.string :name
      t.string :description
      t.datetime :time_spent

      t.timestamps
    end
  end
end
