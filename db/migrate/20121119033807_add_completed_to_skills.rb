class AddCompletedToSkills < ActiveRecord::Migration
  def change
    add_column :skills, :completed, :boolean
  end
end
