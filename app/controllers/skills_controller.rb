class SkillsController < ApplicationController
	respond_to :json

	def index
		respond_with Skill.all
	end

	def show
		respond_with Skill.find(params[:id])
	end

	def create
		respond_with Skill.create(params[:skill])
	end

	def update
		respond_with Skill.update(params[:id], params[:Skill])
	end

	def destroy
		respond_with Skill.destroy(params[:id])
	end
end
