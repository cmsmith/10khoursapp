class Skill < ActiveRecord::Base
  attr_accessible :description, :name, :time_spent
end
